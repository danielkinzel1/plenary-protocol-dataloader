import unittest
from plenaryprotocols.dataloader import load_absences_directory, load_absences_xml, load_speech_xml, \
    load_speeches_directory, load_protocol_date_directory, load_protocol_date_xml
from datetime import datetime


class TestLoadAbscences(unittest.TestCase):
    def test_loadFromXML(self):
        file = load_absences_xml("./test_data/20116-data.xml")
        self.assertEqual(len(file), 46)
        self.assertEqual(file[-1]["name"], "Witt, Uwe")
        self.assertEqual(file[-1]["party"], "fraktionslos")

    def test_loadFromXML_invalid(self):
        pass

    def test_loadFromDirectory(self):
        file = load_absences_directory("./test_data")
        self.assertEqual(len(file), 92)
        self.assertEqual(file[-1]["name"], "Witt, Uwe")
        self.assertEqual(file[-1]["party"], "fraktionslos")


class TestLoadSpeeches(unittest.TestCase):
    def test_loadFromXML(self):
        file = load_speech_xml("./test_data/20116-data.xml")
        self.assertEqual(file[0]["speaker"], "Robert Habeck")
        self.assertTrue(file[0]["speech text"][0:5] == "Frau ")

    def test_loadFromDirectory(self):
        file = load_speeches_directory("./test_data")
        self.assertEqual(file[0]["speaker"], "Robert Habeck")
        self.assertTrue(file[0]["speech text"][0:5] == "Frau ")


class TestLoadPlenaryDates(unittest.TestCase):
    def testloadFromXML(self):
        file = load_protocol_date_xml("./test_data/20116-data.xml")
        self.assertEqual(file, datetime.strptime("07.07.2023", "%d.%m.%Y"))

    def test_loadFromDirectory(self):
        file = load_protocol_date_directory("./test_data")
        self.assertEqual(file[1], datetime.strptime("07.07.2023", "%d.%m.%Y"))
