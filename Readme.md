# What is it
**plenaryprotocols** is a package that provides functions to process and read data from the public available plenary protocol files of the german parliament.
# User Guide

## Installation
Install this package to your python environment using:
```
pip install git+https://gitlab.com/danielkinzel1/plenary-protocol-dataloader
```

## dataloader module
Import desired functions from the module:
```python
from plenaryprotocols.dataloader import load_absences_xml
```

### load speeches
Function to load all speeches from plenary protocol xml files within a directory:
```python
from plenaryprotocols.dataloader import load_speeches_directory
all_speeches = load_speeches_directory("my/sample/directory")

# all_speeches contains:
# [{"speaker": name of the speaker,
#   "speaker id": unique identifier,
#   "date": datetime object,
#   "party": partyname,
#   "speech text": speech text},
#   {...}]
```
Loading of speeches from a dedicated file is possible using ```load_speech_xml("myfilename.xml")```

### load abscences
Function to load all absent parliament members (pm) from plenary protocol xml files within a directory:
```python
from plenaryprotocols.dataloader import load_absences_directory
all_absences = load_absences_directory("my/sample/directory")

# all_absences contains:
# [{"name": pm name,
#   "party": party of the pm,
#   "date": datetime object},
#  {...}]
```
Loading of absence information of a single protocol xml file using ```load_absences_xml("myfilename.xml")```


### load date of plenary appointment
Function to load the date of each parliament appointment for all parliament protocols within a directory:
```python
from plenaryprotocols.dataloader import load_protocol_date_directory
all_dates = load_protocol_date_directory("my/sample/directory")

# all_dates contains:
# [date1, date2, ...]
```
Loading of absence information of a single protocol xml file using ```load_protocol_date_xml("myfilename.xml")```



