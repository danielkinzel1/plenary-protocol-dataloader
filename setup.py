from setuptools import setup, find_packages

setup(
    name='plenaryprotocols',
    version='0.0.1',
    install_requires=[
        'importlib-metadata; python_version >= "3.8"',
    ],
    packages=find_packages()
)
