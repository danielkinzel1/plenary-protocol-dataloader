import warnings
from datetime import datetime
import os
import xml.etree.ElementTree as ET

"""
-------------------------------------------------------------------------------------------------
Data processing functions collection to read and process data from the xml plenary protocol files
-------------------------------------------------------------------------------------------------
"""


def load_absences_directory(directory) -> list:
    """
    Read all xml files in one directory. Load information about absences of members of parliament for each parliament session from a xml file.

    :param directory: path to directory
    :return: list of dictionaries with absences information
    """
    list_absences = []

    files = os.listdir(directory)
    for file in files:
        if file[-3:] == "xml":
            file_path = os.path.join(directory, file)
            if os.path.isfile(file_path):
                try:
                    list_absences += load_absences_xml(file_path)
                except Exception as e:
                    warnings.warn(f"could not read filepath: {file_path}")

    return list_absences


def load_absences_xml(filepath: str) -> list:
    """
    Load information about absences of members of parliament for each parliament session from a xml file.

    :param filepath: path
    :return:list of dictionaries with absences information
    """
    with open(filepath, "r", encoding="UTF-8") as file:
        xml_data = file.read()
    xml_tree = ET.fromstring(xml_data)

    # find date
    date_elem = xml_tree.find(".//vorspann/kopfdaten/veranstaltungsdaten/datum")
    date = date_elem.get("date")

    # find absent mps
    absences = xml_tree.find(".//anlagen-text[@anlagen-typ='Entschuldigte Abgeordnete']")
    table_anchor = absences.find(".//tbody")
    data = []
    for child in table_anchor:
        elems = child.findall("td")
        data.append({"name": postprocessing_text(elems[0].text),
                     "party": postprocessing_text(elems[1].text),
                     "date": date})

    return data


def load_speeches_directory(directory: str) -> list:
    """
    Read all xml files within a directory. Returns tuple with list of created speeches and dataframe representation
    Each speech is represented as dictionary containing speaker, date, content and parliamentary group name

    :param directory: path to directory
    :return: list of speech items
    """
    list_speeches = []

    files = os.listdir(directory)
    for file in files:
        if file[-3:] == "xml":
            file_path = os.path.join(directory, file)
            if os.path.isfile(file_path):
                try:
                    list_speeches += load_speech_xml(file_path)
                except Exception as e:
                    warnings.warn(f"could not read filepath: {file_path}")
    return list_speeches


def load_speech_xml(filepath: str) -> list:
    """
    Load all speeches from one xml file
    Each speech is represented as dictionary containing speaker, date, content and parliamentary group name

    :param filepath: path
    :return: list of speeches
    """
    with open(filepath, "r", encoding="UTF-8") as file:
        xml_data = file.read()
    xml_tree = ET.fromstring(xml_data)
    speeches = []

    # find date
    date_elem = xml_tree.find(".//vorspann/kopfdaten/veranstaltungsdaten/datum")
    date = date_elem.get("date")

    tps = xml_tree.findall('sitzungsverlauf/tagesordnungspunkt')
    for tp in tps:
        spc = tp.findall("rede")
        for speech in spc:
            speech_elements = speech.findall("p")
            _speaker_elem = [u for u in speech_elements if u.get("klasse") == "redner"][0]
            _speaker_name = f"{_speaker_elem.find('.//vorname').text} {_speaker_elem.find('.//nachname').text}"
            _speaker_id = _speaker_elem.find("redner").get("id")

            # get party if also present in XML
            try:
                _speaker_party = _speaker_elem.find('.//fraktion').text
            except AttributeError as e:
                _speaker_party = ""

            # extract speech text
            speech_text = " ".join([u.text for u in speech_elements if u.get("klasse") in {"J", "J_1", "O"}])
            speeches.append({"speaker": postprocessing_text(_speaker_name),
                             "speaker id": _speaker_id,
                             "date": date,
                             "party": postprocessing_text(_speaker_party),
                             "speech text": postprocessing_text(speech_text)})

    return speeches


def load_protocol_date_xml(filepath: str) -> datetime.date:
    """
    Load the datetime of a plenary protocol

    :param filepath: path to xml plenary protocol
    :return: datetime object
    """
    with open(filepath, "r", encoding="UTF-8") as file:
        xml_data = file.read()
    xml_tree = ET.fromstring(xml_data)

    date_elem = xml_tree.find(".//vorspann/kopfdaten/veranstaltungsdaten/datum")
    date = date_elem.get("date")
    date_obj = datetime.strptime(date, "%d.%m.%Y")
    return date_obj


def load_protocol_date_directory(directory: str) -> list[datetime.date]:
    """
    Load all datetimes of all plenary protocols in a dedicated directory

    :param directory: path to directory with xml plenary protocols
    :return: list of datetime objects
    """
    list_dates = []

    files = os.listdir(directory)
    for file in files:
        if file[-3:] == "xml":
            file_path = os.path.join(directory, file)
            if os.path.isfile(file_path):
                try:
                    list_dates += [load_protocol_date_xml(file_path)]
                except Exception as e:
                    warnings.warn(f"could not read filepath: {file_path}")

    return list_dates


def postprocessing_text(text: str) -> str:
    """
    Function to filter out not decoded text fragments

    :param text: raw text
    :return: cleaned text
    """
    replacements = {"\xa0": " ",
                    "\u202f": " ",
                    "\n": " "}
    new_text = text
    for old, new in replacements.items():
        new_text = new_text.replace(old, new)

    # filter for consecutive empty spaces
    while "  " in new_text:
        new_text = new_text.replace("  ", " ")

    return new_text
